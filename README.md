# Docker PHP-Apache
A Dockerfile used to create a custom PHP-apache Docker container for local use.

## Modules
* Headers
* Rewrite

## Extensions
* BC Math
* ImageMagick
* Mysqli
* PDO
* Zip

## Tools
* WP-CLI

## Rights
* Change the UID of www-data to 1000
* Add www-data to group staff
